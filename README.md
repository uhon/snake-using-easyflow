# Snake using Easyflow #

This Assignment uses a simplified version of cmdsnake (simple command line snake game), available at https://github.com/balent/cmdsnake

[Lanterna](https://code.google.com/p/lanterna/) library is used to work with command line interface.

To build application:

    mvn clean install

To run the game:

    mvn exec:java -Dexec.mainClass="cmdsnake.Main"

control keys: arrows
