package cmdsnake;

import au.com.ds.ef.StateEnum;

/**
 * Created with IntelliJ IDEA.
 * User: rbalent
 * Date: 2/26/14
 * Time: 11:07 PM
 * To change this template use File | Settings | File Templates.
 */
public enum SnakeState implements StateEnum {
    MOVING_LEFT, MOVING_RIGHT, MOVING_UP, MOVING_DOWN, RESTING
}
