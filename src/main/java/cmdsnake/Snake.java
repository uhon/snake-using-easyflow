package cmdsnake;

import au.com.ds.ef.StateEnum;
import au.com.ds.ef.StatefulContext;
import au.com.ds.ef.err.LogicViolationError;

import java.util.LinkedList;

import static cmdsnake.SnakeEvent.*;
import static cmdsnake.SnakeState.RESTING;


public class Snake extends StatefulContext {
    private boolean crashed = false;
    StateEnum state = RESTING;

    LinkedList<Pair> body = new LinkedList<Pair>();

    public Snake(int x, int y, int length) {


        for (int i = 0; i < length; i++) {
            body.add(new Pair(x, y));
        }
    }

    public LinkedList<Pair> getBody() {
        return body;
    }


    // TODO: makeSep Method should trigger events to find inapropriate state changes
    public void makeStep(SnakeEvent event) {
        if (crashed) {
            return;
        }


        for (int i = body.size() - 1; i > 0; i--) {
            body.get(i).setX(body.get(i - 1).getX());
            body.get(i).setY(body.get(i - 1).getY());
        }


            if(event != null) {
                // TODO: Trigger event-change on the Snake (Note Snake extends StatefulContext)
                // Catch LogicViolationError which can arise from trigger and write it to System.out
            }



        if (state == SnakeState.MOVING_LEFT) {
            body.get(0).setX(body.get(0).getX() - 1);
        }
        if (state == SnakeState.MOVING_RIGHT) {
            body.get(0).setX(body.get(0).getX() + 1);
        }
        if (state == SnakeState.MOVING_UP) {
            body.get(0).setY(body.get(0).getY() - 1);
        }
        if (state == SnakeState.MOVING_DOWN) {
            body.get(0).setY(body.get(0).getY() + 1);
        }


    }

    public void makeStep() {
        makeStep(null);
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    @Override
    public void setState(StateEnum state) {
        this.state = state;
    }

    public StateEnum getState() {
        return this.state;
    }
}
