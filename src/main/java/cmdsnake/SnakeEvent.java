package cmdsnake;

import au.com.ds.ef.EventEnum;

/**
 * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
 */
public enum SnakeEvent implements EventEnum {
    LEFT,
    UP,
    RIGHT,
    DOWN
}
