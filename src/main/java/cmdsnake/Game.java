package cmdsnake;

import au.com.ds.ef.EventEnum;
import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenWriter;
import com.googlecode.lanterna.terminal.Terminal;

import java.awt.*;
import java.util.LinkedList;

public class Game {

    Terminal terminal;
    Screen screen;
    ScreenWriter writer;
    private Snake snake;
    private LinkedList<SnakeEvent> eventQueue;
    private Key[] keys = {
            new Key(Key.Kind.ArrowLeft), new Key(Key.Kind.ArrowUp), new Key(Key.Kind.ArrowRight), new Key(Key.Kind.ArrowDown)
    };

    public Game() {
        terminal = TerminalFacade.createTerminal();

        screen = new Screen(terminal);

        screen.setCursorPosition(null); // Hack to hide command line cursor
        screen.startScreen();

        screen.refresh();

        writer = new ScreenWriter(screen);


        snake = new Snake(15, 20, 12);

        // TODO: Initialize SnakeStateHandler, get the flow and start it


        eventQueue = new LinkedList<SnakeEvent>();
    }

    public void start() {

        drawScreen();

        long stepStartMillis = 0;

        while (true) {

            Key key = terminal.readInput();

            if (key != null) {
                SnakeEvent event = getEventFromKey(key);

                if (event != null) {

                    if (eventQueue.size() == 0) {
//                        if (snake.getDirection() != getOppositeDirection(direction)) {
                        eventQueue.add(event);
//                        }
                    } else { /*if (eventQueue.getLast() != getOppositeDirection(direction)) {   */
                        eventQueue.add(event);
                    }
                }
            }

            long currentMillis = System.currentTimeMillis();
            if (currentMillis - stepStartMillis > 250) {
                stepStartMillis = currentMillis;


                if (eventQueue.size() > 0) {
                    snake.makeStep(eventQueue.remove());
                } else {
                    snake.makeStep();
                }

                drawScreen();

                Pair snakeHead = snake.getBody().getFirst();
                // Check for collision
                if(snake.getState() != SnakeState.RESTING) {
                    for (int i = 1; i < snake.getBody().size(); i++) {
                        if (snake.getBody().get(i).equals(snakeHead)) {
                            snake.setCrashed(true);
                        }
                    }
                }

                if (snakeHead.getX() == 0 || snakeHead.getY() == 0 || snakeHead.getX() == 79 || snakeHead.getY() == 23) {
                    snake.setCrashed(true);
                }
            }
            if (snake.isCrashed()) {
                writer.drawString(20, 10, "##############################");
                writer.drawString(20, 11, "#         Game over!         #");
                writer.drawString(20, 12, "##############################");
                screen.refresh();
                try {
                    Thread.sleep(3000);
                    screen.stopScreen();
                    System.exit(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void drawScreen() {
        screen.clear();

        writer.drawString(0, 0, "################################################################################");
        for (int i = 1; i < 23; i++) {
            writer.drawString(0, i, "#");
            writer.drawString(79, i, "#");
        }
        writer.drawString(0, 23, "################################################################################");


        for (int i = 1; i < snake.getBody().size(); i++) {
            writer.drawString(snake.getBody().get(i).getX(), snake.getBody().get(i).getY(), "X");
        }

        writer.drawString(snake.getBody().get(0).getX(), snake.getBody().get(0).getY(), "@");

        screen.refresh();
    }

    private SnakeEvent getEventFromKey(Key key) {
        for (int j = 0; j < keys.length; j++) {
            if (keys[j].equals(key)) {
                return SnakeEvent.values()[j];
            }
        }
        return null;
    }





    private boolean isOccupied(Pair pair) {
        if (pair.getX() == 0 || pair.getY() == 0 || pair.getX() == 79 || pair.getY() == 23) {
            return true;
        }

        if (snake.getBody().contains(pair)) {
            return true;
        }

        return false;
    }

}
