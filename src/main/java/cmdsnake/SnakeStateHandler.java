package cmdsnake;

import static au.com.ds.ef.FlowBuilder.from;
import static au.com.ds.ef.FlowBuilder.on;
import static cmdsnake.SnakeEvent.*;
import static cmdsnake.SnakeState.*;

import au.com.ds.ef.EasyFlow;
import au.com.ds.ef.SyncExecutor;

/**
 * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
 */
public class SnakeStateHandler {

    private EasyFlow<Snake> flow =

        // TRANSITIONS
        from(RESTING).transit(
                on(RIGHT).to(MOVING_RIGHT)
                // TODO: build additional State-Transitions here
        )


        // EVENTS
        .whenEvent(LEFT, (Snake ctx) -> {
            System.out.println("SnakeTransition - EVENT: " + LEFT);
        })
        // TODO: Add event-log for other events

                        // SYNC EXECUTION
        .executor(new SyncExecutor());



    public EasyFlow getFlow() {
        return flow;
    }



}
